ARG DVER=latest
FROM docker.io/alpine:$DVER
#ENV APKVER=0.5.1-r2

WORKDIR /usr/local/bin
COPY docker-entrypoint.sh get-label-info.sh ./

RUN apk add --no-cache docker-cli-buildx curl

#WORKDIR /tmp
#RUN apk add --no-cache --virtual .build-deps openssl \
#&& echo " => Creating ca.srl" \
#&& echo 01 > ca.srl \
#&& echo " => Generating CA key" \
#&& openssl genrsa  -out ca-key.pem 2048 \
#&& echo " => Generating CA certificate" \
#&& openssl req \
#  -new \
#  -key ca-key.pem \
#  -x509 \
#  -days 3650 \
#  -nodes \
#  -subj "/CN=docker" \
#  -out ca.pem \
#&& echo " => Generating server key" \
#&& openssl genrsa -out server-key.pem 2048 \
#&& echo " => Generating server CSR" \
#&& openssl req \
#  -subj "/CN=$DOCKER_HOST" \
#  -new \
#  -key server-key.pem \
#  -out server.csr \
#&& echo " => Signing server CSR with CA" \
#&& openssl x509 \
#  -req \
#  -days 3650 \
#  -in server.csr \
#  -CA ca.pem \
#  -CAkey ca-key.pem \
#  -out server-cert.pem \
#&& echo " => Generating client key" \
#&& openssl genrsa \
#  -out key.pem 2048 \
#&& echo " => Generating client CSR" \
#&& openssl req \
#  -subj "/CN=docker.client" \
#  -new \
#  -key key.pem \
#  -out client.csr \
#&& echo " => Creating extended key usage" \
#&& echo extendedKeyUsage = clientAuth > extfile.cnf \
#&& echo " => Signing client CSR with CA" \
#&& openssl x509 \
#  -req \
#  -days 3650 \
#  -in client.csr \
#  -CA ca.pem \
#  -CAkey ca-key.pem \
#  -out cert.pem \
#  -extfile extfile.cnf \
#&& mkdir -p /certs \
#&& cp -a ca.pem cert.pem key.pem /certs/ \
#&& chmod -v 400 /certs/*.pem \
#&& rm -rf /tmp/* \
#&& apk del .build-deps \
#&& ls -lah /certs

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
CMD ["sh"]
