#!/bin/sh
set -eu

# if DOCKER_HOST isn't set (no custom setting, no default socket), let's set it to a sane remote value
if [ -z "${DOCKER_HOST:-}" ]; then
	export DOCKER_HOST='tcp://docker:2375'
fi

#export DOCKER_TLS_VERIFY=1

exec "$@"
